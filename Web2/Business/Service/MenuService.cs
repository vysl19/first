﻿using Business.Helper;
using Data;
using Data.Entity;
using Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public interface IMenuService
    {
        List<IMenu> List();

        void Upsert(IMenu menu);
    }
    public class MenuService : IMenuService
    {
        private readonly IMenuRepository _menuRepository;
        public MenuService(IMenuRepository menuRepository)
        {
            _menuRepository = menuRepository;
        }

        public void Upsert(IMenu menu)
        {
            _menuRepository.Upsert(menu);
        }

        public List<IMenu> List()
        {           
            return _menuRepository.List();
        }
    }
}
