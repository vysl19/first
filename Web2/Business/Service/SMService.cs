﻿using Data.Enity;
using Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Service
{
    public interface ISMService
    {
        List<ISM> List();

        void Upsert(ISM sm);
    }

    public class SMService : ISMService
    {
        private readonly IStringRepository _stringRepository;
        public SMService(IStringRepository stringRepository)
        {
            _stringRepository = stringRepository;
        }

        public List<ISM> List()
        {
            return _stringRepository.List();
        }

        public void Upsert(ISM sm)
        {
            _stringRepository.Upsert(sm);
        }
    }
}
