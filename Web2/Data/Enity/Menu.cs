﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entity
{
    public interface IMenu
    {
        /// <summary>
        /// Id
        /// </summary>
        int Id { get; set; }
        /// <summary>
        /// Order
        /// </summary>
        int Order { get; set; }
        /// <summary>
        /// Title
        /// </summary>
        string Title { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        string Description { get; set; }
        /// <summary>
        /// Html
        /// </summary>
        string Html { get; set; }

        /// <summary>
        /// SubMenus Title
        /// </summary>
        List<string> SubMenus { get; set; }

        /// <summary>
        /// Menu Tipi iki tane dinamik menu mevcut oldugu için boyle eklenildi
        /// </summary>
        MenuType MenuType { get; set; }

        /// <summary>
        /// ImageUrl
        /// </summary>
        string ImageUrl { get; set; }
    }

    public enum MenuType
    {
        Service,
        Sector
    }

    public class Menu : IMenu
    {
        public Menu()
        {
            SubMenus = new List<string>();
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Order
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// SubMenus
        /// </summary>
        public List<string> SubMenus { get; set; }

        public string Html { get; set; }
        /// <summary>
        /// Menu Tipi iki tane dinamik menu mevcut oldugu için boyle eklenildi
        /// </summary>
        public MenuType MenuType { get; set; }

        /// <summary>
        /// ImageUrl
        /// </summary>        
        public string ImageUrl { get; set; }
    }
    //design changed
    //public interface IMenu
    //{
    //    int Id { get; set; }
    //    int ParentId { get; set; }
    //    string Name { get; set; }
    //    int Order { get; set; }
    //    string Description { get; set; }
    //    bool IsDynamic { get; set; }
    //    string Html { get; set; }
    //}

    //public class Menu : IMenu
    //{
    //    /// <summary>
    //    /// Id
    //    /// </summary>
    //    public int Id { get; set; }

    //    /// <summary>
    //    /// ParentId
    //    /// </summary>
    //    public int ParentId { get; set; }

    //    /// <summary>
    //    /// Name
    //    /// </summary>
    //    public string Name { get; set; }

    //    /// <summary>
    //    /// Order
    //    /// </summary>
    //    public int Order { get; set; }

    //    /// <summary>
    //    /// Description
    //    /// </summary>
    //    public string Description { get; set; }

    //    /// <summary>
    //    /// Has Dynamic Html or static html 
    //    /// </summary>
    //    public bool IsDynamic { get; set; }

    //    /// <summary>
    //    /// Html
    //    /// </summary>
    //    public string Html { get; set; }
    //}
}
