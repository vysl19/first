﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Enity
{
    public interface ISM
    {
        /// <summary>
        /// STringCode
        /// </summary>
        string StringCode { get; set; }

        /// <summary>
        /// StringValue
        /// </summary>
        string StringValue { get; set; }

        /// <summary>
        /// LanguageCode
        /// </summary>
        string LanguageCode { get; set; }
    }
    public class SM : ISM
    {
        public string StringCode { get; set; }
        public string StringValue { get; set; }
        public string LanguageCode { get; set; }
    }
}
