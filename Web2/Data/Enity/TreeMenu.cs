﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entity
{
    public interface ITreeMenu
    {
        IMenu Item { get; set; }
        List<ITreeMenu> Childreen { get; set; }
    }
    public class TreeMenu:ITreeMenu
    {
        /// <summary>
        /// Item
        /// </summary>
        public IMenu Item { get; set; }
        /// <summary>
        /// Childreen
        /// </summary>
        public List<ITreeMenu> Childreen { get; set; }

        public TreeMenu()
        {
            Childreen = new List<ITreeMenu>();
        }
    }
}
