﻿using Data.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public interface IMenuRepository
    {
        List<IMenu> List();
        void Upsert(IMenu menu);
    }
    public class MenuRepository : BaseRepository, IMenuRepository
    {
        public MenuRepository() : base()
        {

        }
        /// <summary>
        /// Menu bilgisi listler
        /// </summary>
        /// <returns></returns>
        public List<IMenu> List()
        {
            var result = new List<IMenu>();
            using (conn)
            {
                using (cmd)
                {
                    conn.Open();
                    cmd.CommandText = "dbo.MENU_SELECT";
                }
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            IMenu entity = new Menu
                            {
                                Id = int.Parse(reader["ID"].ToString()),
                                Html = reader["HTML"].ToString(),
                                SubMenus = reader["SUB_MENUS"].ToString().Split('|').ToList(),
                                Order = int.Parse(reader["MENU_ORDER"].ToString()),
                                Title = reader["NAME"].ToString(),
                                Description = reader["DESCRIPTION"].ToString(),
                                ImageUrl = reader["IMAGE_URL"].ToString(),
                                MenuType = (MenuType)int.Parse(reader["MENU_TYPE"].ToString())
                            };
                            result.Add(entity);
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// New Menu update eder veya insert eder
        /// </summary>
        /// <returns></returns>
        public void Upsert(IMenu menu)
        {
            using (conn)
            {
                using (cmd)
                {
                    conn.Open();
                    cmd.CommandText = "dbo.MENU_UPSERT";
                    cmd.Parameters.AddWithValue("@ID", menu.Id);
                    cmd.Parameters.AddWithValue("@TITLE", menu.Title);
                    cmd.Parameters.AddWithValue("@DESCRIPTION", menu.Description);
                    cmd.Parameters.AddWithValue("@SUB_MENUS", string.Join("|", menu.SubMenus));
                    cmd.Parameters.AddWithValue("@HTML", menu.Html);
                    cmd.Parameters.AddWithValue("@IMAGE_URL", menu.ImageUrl);
                    cmd.Parameters.AddWithValue("@ORDER", menu.Order);
                    cmd.Parameters.AddWithValue("@MENU_TYPE", menu.MenuType);
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
