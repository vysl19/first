﻿using Data.Enity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public interface IStringRepository
    {
        List<ISM> List();
        void Upsert(ISM obj);
    }
    public class StringRepository : BaseRepository, IStringRepository
    {         
        public List<ISM> List()
        {
            var result = new List<ISM>();
            using (conn)
            {
                using (cmd)
                {
                    conn.Open();
                    cmd.CommandText = "dbo.STRING_SELECT";
                }
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            ISM entity = new SM
                            {                                
                                StringCode = reader["STRING_CODE"].ToString(),
                                StringValue = reader["STRING_VALUE"].ToString(),
                                LanguageCode = reader["LANGUAGE_CODE"].ToString(),
                            };
                            result.Add(entity);
                        }
                    }
                }
            }

            return result;
        }

        public void Upsert(ISM sm)
        {
            using (conn)
            {
                using (cmd)
                {
                    conn.Open();
                    cmd.CommandText = "dbo.STRING_UPSERT";                    
                    cmd.Parameters.AddWithValue("@STRING_CODE", sm.StringCode);
                    cmd.Parameters.AddWithValue("@STRING_VALUE", sm.StringValue);
                    cmd.Parameters.AddWithValue("@LANGUAGE_CODE", sm.LanguageCode);
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
