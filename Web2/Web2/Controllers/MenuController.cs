﻿using Business;
using Data.Entity;
using System.Collections.Generic;
using System.Web.Http;

namespace Web2.Controllers
{
    public class MenuController : ApiController
    {
        private readonly IMenuService _menuService;
        public MenuController(IMenuService menuService)
        {
            _menuService = menuService;
        }
        [HttpGet]
        public List<IMenu> List()
        {
            return _menuService.List();
        }
        [HttpPost]
        public void Upsert([FromBody]IMenu menu)
        {
            _menuService.Upsert(menu);
        }
    }
}
