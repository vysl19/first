﻿using Business.Service;
using Data.Enity;
using System.Collections.Generic;
using System.Web.Http;

namespace Web2.Controllers
{
    public class StringController : ApiController
    {
        private readonly ISMService _smService;
        public StringController(ISMService smService)
        {
            _smService = smService;
        }
        [HttpGet]
        public List<ISM> List()
        {
            return _smService.List();
        }
        [HttpPost]
        public void Upsert([FromBody]ISM sm)
        {
            _smService.Upsert(sm);
        }
    }
}
