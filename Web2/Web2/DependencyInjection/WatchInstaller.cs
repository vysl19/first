﻿using Business;
using Business.Service;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Data.Enity;
using Data.Entity;
using Data.Repository;
using System.Web.Http.Controllers;

namespace Web2.DependencyInjection
{
    public class WatchInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly().BasedOn<IHttpController>().LifestylePerWebRequest());

            container.Register(Component.For<IMenuService>().LifestylePerWebRequest().ImplementedBy<MenuService>());
            container.Register(Component.For<IMenu>().LifestylePerWebRequest().ImplementedBy<Menu>());
            container.Register(Component.For<IMenuRepository>().LifestylePerWebRequest().ImplementedBy<MenuRepository>());

            container.Register(Component.For<ISMService>().LifestylePerWebRequest().ImplementedBy<SMService>());
            container.Register(Component.For<ISM>().LifestylePerWebRequest().ImplementedBy<SM>());
            container.Register(Component.For<IStringRepository>().LifestylePerWebRequest().ImplementedBy<StringRepository>());
            
        }
    }
}