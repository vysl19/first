﻿var selected;
var datatableId;
var productTypes = [];
$(function () {
    CloseOtherPanelsAndOpenPanel("divSlider");
    $("#aSlider").click(function () {
        CloseOtherPanelsAndOpenPanel("divSlider");
        LoadSlider();
    });
    $("#aProductType").click(function () {
        CloseOtherPanelsAndOpenPanel("divProductType");
        LoadProductType();
    });

    $("#aProduct").click(function () {
        CloseOtherPanelsAndOpenPanel("divProduct");
        LoadProductTypeSelect();
    });
    $("#aAbout").click(function () {
        CloseOtherPanelsAndOpenPanel("divAbout");   
        LoadAbout();
    }); 
    $("#aContact").click(function () {
        CloseOtherPanelsAndOpenPanel("divContact");  
        LoadContact();
    });
    $("#aReference").click(function () {
        CloseOtherPanelsAndOpenPanel("divReference");
        LoadReference();
    });

    $("#aboutSave").click(function () {        
        var src = $("a.pop img").prop("src");
        var data = {};        
        data.Content = src;
        data.Text = $("#TextForAbout").val();
        $.post("/api/About", data, function () {
        })
            .done(function (response) {
                alert("Kaydedildi");
            })
            .fail(function (e) {

            });
    });
    $("#contactSave").click(function () { 
        var data = {};
        data.Address = $("#TextForAddress").val();
        data.PhoneNumber = $("#TextForPhoneNumber").val();
        data.EmailAddress = $("#TextForEmail").val();
        data.Location = $("#TextForLocation").val();        
        data.Instagram = $("#TextForInstagram").val(); 
        data.Facebook = $("#TextForFacebook").val(); 
        data.Twitter = $("#TextForTwitter").val();         
        $.post("/api/Contact", data, function () {
        })
            .done(function (response) {
                alert("Kaydedildi");
            })
            .fail(function (e) {

            });
    });

    $("#SelectProductType").change(function () {
        var option = $(this).find("option:selected");
        LoadProduct(option.prop("value"));
    });


    LoadSlider();
    //Slider 
    $(document).on('change', '.custom-file-input', function () {
        if ($(this).prop("id") == "aboutInputFile") {
            var reader = new FileReader();
            reader.readAsArrayBuffer(this.files[0]);

            reader.onloadend = function () {                
                $("a.pop img").prop("src", "data:image/jpeg;base64," + arrayBufferToBase64(this.result));                
            }
        }
        else {
            selected = $(this).closest("tr").index();
            var reader = new FileReader();
            reader.readAsArrayBuffer(this.files[0]);

            reader.onloadend = function () {
                var data = $("#" + datatableId).DataTable().row(selected).data();
                data.ImageUrl = "data:image/jpeg;base64," + arrayBufferToBase64(this.result);
                $("#" + datatableId).DataTable().row(selected).data(data).draw();
            }
        }
    });

    $(document).on("click", 'a.pop', function () {
        var imgSrc = $(this).find("img").prop("src");
        $('#imagepreview').attr('src', imgSrc); // here asign the image to the modal when the user click the enlarge link
        $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
    });
    $(document).on("click", 'button.save', function () {
        var tr = $(this).closest("tr");
        var index = tr.index();
        var src = tr.find("img").prop("src");
        var data = {};
        data.Id = $("#" + datatableId).DataTable().row(index).data().Id;
        data.Content = src;
        data.Text = tr.find(".TextInput").find("input").val();
        var postPath = "";
        switch (datatableId) {
            case "dataTable":
                postPath = '/api/Slider';
                break;
            case "dataTableForProductType":
                postPath = '/api/ProductType';
                break;
            case "dataTableForReference":
                postPath = "/api/Reference";
                break;
            case "dataTableForProduct":                
                postPath = '/api/Product';
                data.RowNumber = tr.find('.SiraNumara').find('input').val();
                data.ProductTypeId = $("#SelectProductType").find("option:selected").prop("value");
                break;
        }
        
        $.post(postPath, data, function () {
        })
            .done(function (response) {
                alert("Kaydedildi");
            })
            .fail(function (e) {

            });
    });
    $(document).on("change", 'input.TextInput', function () {
        var index = $(this).closest("tr").index()
        var text = $(this).val();
        var data = $("#" + datatableId).DataTable().row(index).data();
        data.Text = text;
        $("#" + datatableId).DataTable().row(index).data(data).draw();
    });

});
function arrayBufferToBase64(buffer) {
    let binary = '';
    const bytes = new Uint8Array(buffer);
    const len = bytes.byteLength;
    for (let i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
};
function LoadProduct(id) {
    datatableId = "dataTableForProduct";
    $.get('/api/Product/' + id, function () {
    })
        .done(function (response) {            
            $("#" + datatableId).DataTable({
                paging: false,
                searching: false,
                destroy: true,
                info: false,
                order: true,                
                "aaData": response,
                "columns": [
                    {
                        "data": "Id", "orderable": false, "visible": false
                    }
                    , { "data": "ImageUrl", "orderable": false, "className": "imgUrl" }
                    , { "data": "Text", "orderable": true, "className": "TextInput" }
                    , { "data": "ImageUrl", "orderable": true }
                    , { "data": "RowNumber", "className": "SiraNumara", "orderable": true }
                    , { "data": "Id" }
                ],
                columnDefs: [
                    {
                        targets: 1,
                        "data": "img",
                        render: function (data) {
                            var text = "<a href='#' class='pop'>";
                            text += '<img class="img-responsive" width="200px" height="200px" src="' + data + '"></a>';
                            return text;
                        }
                    },
                    {
                        targets: 2,
                        render: function (data) {
                            text = '<input type="text" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" value="' + data + '"/>';
                            return text;
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, full) {
                            var text = "";
                            text += "<div class='input-group mb-3'>";
                            text += "<div class='custom-file'>";
                            text += "<input type='file' class='custom-file-input'  aria-describedby='fileHelp'/>";
                            text += "<label class='custom-file-label' for='exampleInputFile'>" + "Dosya Sec" + "</label></div>";
                            text += "<div class='input-group-append'>";
                            text += "</div>";
                            return text;
                        }
                    },
                    {
                        targets: -2,
                        render: function (data, type, full) {
                            var text = "";
                            text += '<div class="input-group mb-3">';
                            text += '<input type="number" class="form-control" placeholder="SıraNumarası" aria-label="SıraNumarası" aria-describedby="basic-addon1" value="' + data + '" min="0" data-bind="value:replyNumber"></div>';
                            return text;                              
                        }
                    },
                    {
                        targets: -1,
                        render: function (data, type, full) {
                            return '<button type="button" class="btn btn-secondary save">Kaydet</button>';

                        }
                    }
                ]
            });
        })
        .fail(function () {

        });
}


function LoadProductType() {
    datatableId = "dataTableForProductType";
    $.get('/api/ProductType', function () {
    })
        .done(function (response) {
            $("#" + datatableId).DataTable({
                paging: false,
                searching: false,
                destroy: true,
                info: false,
                order: false,
                "aaData": response,
                "columns": [
                    {
                        "data": "Id", "orderable": false, "visible": false
                    }
                    , { "data": "ImageUrl", "orderable": false, "className": "imgUrl" }
                    , { "data": "Text", "orderable": true, "className": "TextInput" }
                    , { "data": "ImageUrl", "orderable": true }
                    , { "data": "Id" }
                ],
                columnDefs: [
                    {
                        targets: 1,
                        "data": "img",
                        render: function (data) {
                            var text = "<a href='#' class='pop'>";
                            text += '<img class="img-responsive" width="200px" height="200px" src="' + data + '"></a>';
                            return text;
                        }
                    },
                    {
                        targets: 2,
                        render: function (data) {
                            text = '<input type="text" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" value="' + data + '"/>';
                            return text;
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, full) {
                            var text = "";
                            text += "<div class='input-group mb-3'>";
                            text += "<div class='custom-file'>";
                            text += "<input type='file' class='custom-file-input'  aria-describedby='fileHelp'/>";
                            text += "<label class='custom-file-label' for='exampleInputFile'>" + "Dosya Sec" + "</label></div>";
                            text += "<div class='input-group-append'>";
                            text += "</div>";
                            return text;
                        }
                    },
                    {
                        targets: -1,
                        render: function (data, type, full) {
                            return '<button type="button" class="btn btn-secondary save">Kaydet</button>';

                        }
                    }
                ]
            });
        })
        .fail(function () {

        });
}
function LoadSlider() {
    datatableId = "dataTable";
    $.get('/api/Slider', function () {
    })
        .done(function (response) {
            if (response.length < 3) {
                for (var i = 0; i < 3 - response.length; i++) {
                    var data = { Id: -1, ImageUrl: "", Text: "" };
                    response.push(data);
                }
            }
            $("#" + datatableId).DataTable({
                paging: false,
                searching: false,
                destroy: true,
                info: false,
                "aaData": response,
                "columns": [
                    {
                        "data": "Id", "orderable": false, "visible": false                    }
                    , { "data": "ImageUrl", "orderable": false, "className": "imgUrl" }
                    , { "data": "Text", "orderable": true, "className": "TextInput" }
                    , { "data": "ImageUrl", "orderable": true }
                    , { "data": "Id" }
                ],
                columnDefs: [
                    {
                        targets: 1,
                        "data": "img",
                        render: function (data) {
                            var text = "<a href='#' class='pop'>";
                            text += '<img class="img-responsive" width="200px" height="200px" src="' + data + '"></a>';
                            return text;
                        }
                    },
                    {
                        targets: 2,
                        render: function (data) {
                            text = '<input type="text" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" value="' + data + '"/>';
                            return text;
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, full) {
                            var text = "";
                            text += "<div class='input-group mb-3'>";
                            text += "<div class='custom-file'>";
                            text += "<input type='file' class='custom-file-input'  aria-describedby='fileHelp'/>";
                            text += "<label class='custom-file-label' for='exampleInputFile'>" + "Dosya Sec" + "</label></div>";
                            text += "<div class='input-group-append'>";
                            text += "</div>";
                            return text;
                        }
                    },
                    {
                        targets: -1,
                        render: function (data, type, full) {
                            return '<button type="button" class="btn btn-secondary save">Kaydet</button>';

                        }

                    }
                ]
            });
        })
        .fail(function () {
            alert("error");
        });

}
function CloseOtherPanelsAndOpenPanel(panelId) {
    $(".container-fluid").addClass("collapse");
    $("#" + panelId).removeClass("collapse in");

}
function AddProductType() {
    $("#dataTableForProductType").DataTable().row.add({
        "Id": -1, "ImageUrl": "", "Text": ""
    }).draw();

}
function AddProduct() {
    $("#dataTableForProduct").DataTable().row.add({
        "Id": -1, "ImageUrl": "", "Text": "", "RowNumber":"0"
    }).draw();
}
function LoadProductTypeSelect() {
    $.get('/api/ProductType', function () {
    })
        .done(function (response) {
            var select = $("#SelectProductType");
            select.html('');
            select.append('<option value = "-1" disabled selected>Seç</option >');
            $.each(response, function (i) {
                var text = "<option value='" + response[i].Id + "'>" + response[i].Text + "</option>";
                select.append(text);
            });

        })
        .fail(function () {
            alert("error");
        });
}
function LoadAbout() {
    $.get('/api/About/', function () {
    })
        .done(function (response) {
            $("a.pop img").prop("src", response.ImageUrl);
            $("#TextForAbout").val(response.Text);
        })
        .fail(function () {
            alert("error");
        });
}

function LoadContact() {
    $.get('/api/Contact/', function () {
    })
        .done(function (response) {
            //$("a.pop img").prop("src", response.ImageUrl);            
            $("#TextForAddress").val(response.Address);
            $("#TextForPhoneNumber").val(response.PhoneNumber);
            $("#TextForEmail").val(response.EmailAddress);
            $("#TextForLocation").val(response.Location);
            $("#TextForInstagram").val(response.Instagram);
            $("#TextForFacebook").val(response.Facebook);
            $("#TextForTwitter").val(response.Twitter);
        })
        .fail(function () {
            alert("error");
        });
}

function LoadReference() {
    datatableId = "dataTableForReference";
    $.get('/api/Reference', function () {
    })
        .done(function (response) {
            $("#" + datatableId).DataTable({
                paging: false,
                searching: false,
                destroy: true,
                info: false,
                order: false,
                "aaData": response,
                "columns": [
                    {
                        "data": "Id", "orderable": false, "visible": false
                    }
                    , { "data": "ImageUrl", "orderable": false, "className": "imgUrl" }
                    , { "data": "Text", "orderable": true, "className": "TextInput" }
                    , { "data": "ImageUrl", "orderable": true }
                    , { "data": "Id" }
                ],
                columnDefs: [
                    {
                        targets: 1,
                        "data": "img",
                        render: function (data) {
                            var text = "<a href='#' class='pop'>";
                            text += '<img class="img-responsive" width="200px" height="200px" src="' + data + '"></a>';
                            return text;
                        }
                    },
                    {
                        targets: 2,
                        render: function (data) {
                            text = '<input type="text" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" value="' + data + '"/>';
                            return text;
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, full) {
                            var text = "";
                            text += "<div class='input-group mb-3'>";
                            text += "<div class='custom-file'>";
                            text += "<input type='file' class='custom-file-input'  aria-describedby='fileHelp'/>";
                            text += "<label class='custom-file-label' for='exampleInputFile'>" + "Dosya Sec" + "</label></div>";
                            text += "<div class='input-group-append'>";
                            text += "</div>";
                            return text;
                        }
                    },
                    {
                        targets: -1,
                        render: function (data, type, full) {
                            return '<button type="button" class="btn btn-secondary save">Kaydet</button>';

                        }
                    }
                ]
            });
        })
        .fail(function () {

        });
}